class Company < ActiveRecord::Base
  has_many :payments
  belongs_to :user

  validates :name, presence: true, length: { minimum: 2 }

end
