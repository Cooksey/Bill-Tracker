class Payment < ActiveRecord::Base
  belongs_to :company
  belongs_to :payment_method

  validates :company_id, presence: true
  validates :payment_method_id, presence: true
  validates :amount, presence: true
  validates :date, presence: true
end
