describe 'the companies management process' do

  subject { page }

  before :each do
    user = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
  end

  context 'index page' do

    before :each do
      visit companies_path
    end

    it { should have_link('New Company') }
    it { should have_selector('.table') }

    it 'should go to new company page' do
      click_on 'New Company'
      expect(current_path).to eq(new_company_path)
    end

  end

  context 'creating new company' do

    before :each do
      visit new_company_path
    end

    it { should have_content 'Add a company' }

    it 'should create new company' do
      fill_in 'company[name]', :with => 'ABC Corp'
      fill_in 'Description', :with => 'Company description.'
      click_button 'Create Company'

      within('.alert-success') do
        expect(page).to have_content 'Company was successfully created.'
      end
    end

    context 'when failure' do
      it 'should not accept empty company name' do
        fill_in 'company[name]', :with => ''
        fill_in 'Description', :with => ''
        click_button 'Create Company'

        within('.alert') do
          expect(page).to have_content '2 errors prohibited this company from being saved:'
          expect(page).to have_content "Name can't be blank"
          expect(page).to have_content 'Name is too short (minimum is 2 characters)'
        end

      end
    end
  end

end