describe 'the payment method management process' do

  subject { page }

  before :each do
    user = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
  end

  context 'index page' do

    before :each do
      visit payment_methods_url
    end

    it { should have_link('New Payment method') }
    it { should have_selector('.table') }

    it 'should go to new company page' do
      click_on 'New Payment method'
      expect(current_path).to eq(new_payment_method_path)
    end

  end

  context 'adding new payment method' do

    before :each do
      visit new_payment_method_path
    end

    it { should have_content 'Add payment method' }

    it 'should create new company' do
      fill_in 'payment_method_name', :with => 'Visa MasterCard'
      fill_in 'payment_method_description', :with => 'Payment method description.'
      click_button 'Create Payment method'

      within('.alert-success') do
        expect(page).to have_content 'Payment method was successfully created.'
      end
    end

    context 'when failure' do
      it 'should not accept empty company name' do
        fill_in 'payment_method_name', :with => ''
        fill_in 'payment_method_description', :with => ''
        click_button 'Create Payment method'

        within('.alert') do
          expect(page).to have_content '2 errors prohibited this company from being saved:'
          expect(page).to have_content "Name can't be blank"
          expect(page).to have_content 'Name is too short (minimum is 2 characters)'
        end

      end
    end

  end

end