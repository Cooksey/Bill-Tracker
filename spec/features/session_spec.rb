describe 'the signin process' do

  subject { page }

  let(:user)          { {email: 'user@example.com', password: 'password'} }
  let(:invalid_user)  { {email: 'bob@aol.com', password: 'abcd838383'} }

  before :each do
    User.create(:email => user[:email], :password => user[:password])
  end

  context 'when success' do

    before :each do
      visit new_user_session_path
      fill_in 'Email', :with => user[:email]
      fill_in 'Password', :with => user[:password]
      click_button 'Log in'
    end

    it { should have_content 'Signed in successfully.' }
    it { should have_selector('.navbar-brand') }
    it { should have_selector('.navbar-nav') }

  end

  context 'when failure' do

    before :each do
      visit new_user_session_path
      fill_in 'Email', :with => invalid_user[:email]
      fill_in 'Password', :with => invalid_user[:password]
      click_button 'Log in'
    end

    it { should have_content 'Invalid email address or password.' }
  end

end