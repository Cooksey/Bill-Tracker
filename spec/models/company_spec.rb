require 'spec_helper'

describe Company do
  it { should have_many :payments }
  it { should belong_to :user }

  context 'with no name' do
    subject { Company.create(:name => '').valid? }
    it { should be_falsey }
  end

  context 'with 1 letter name' do
    subject { Company.create(:name => 'a').valid? }
    it { should be_falsey }
  end

  context 'with 2 letter name' do
    subject { Company.create(:name => 'ab').valid? }
    it { should be_truthy }
  end

end
