require 'spec_helper'

describe Payment do

  it { should belong_to :company }
  it { should belong_to :payment_method }

  let(:company) { 1 }
  let(:payment_method_id) { 1 }
  let(:amount) { 29.99 }
  let(:date) { Time.now }

  let(:payment) { {
      company_id: company,
      payment_method_id: payment_method_id,
      amount: amount,
      date: date
    }
  }

  context 'valid' do
    subject { Payment.create(payment).valid? }
    it { should be_truthy }
  end

  context 'without company' do
    let(:company) { nil }
    subject { Payment.create(payment).valid? }
    it { should be_falsey }
  end

  context 'without payment method' do
    let(:payment_method_id) { nil }
    subject { Payment.create(payment).valid? }
    it { should be_falsey }
  end

  context 'without amount' do
    let(:amount) { nil }
    subject { Payment.create(payment).valid? }
    it { should be_falsey }
  end

  context 'without date' do
    let(:date) { nil }
    subject { Payment.create(payment).valid? }
    it { should be_falsey }
  end

end
